const https = require('https');
const fs = require('fs');

const out = [];
const argv = process.argv;

const pluginName = argv[2];
const repoUrl = argv[3];
const gitTag = argv[4];

function getFile(url) {
	return new Promise(resolve => {
		https.get(url, res => {
			let data = '';
			res.on('data', chunk => {
				data += chunk;
			});
			res.on('end', () => {
				resolve(data);
			})
		})
		.on('error', err => {
			console.log(err.message);
			resolve(null);
		})
	})
}

(async () => {
	// https://gitlab.com/kaminariss/nextui-plugin/-/raw/master/.gitlab-ci.yml
	const manifestUrl = repoUrl.trim() + `/-/raw/${gitTag}/${ pluginName }/${ pluginName }.json`;
	const response = await getFile(manifestUrl);
	const manifest = JSON.parse(response);

	const lastUpdated = Math.round(new Date().valueOf() / 1000);

	const link = `${ repoUrl }/-/jobs/artifacts/${gitTag}/raw/${ pluginName }/bin/Release/${ pluginName }/latest.zip?job=build`
	const icon = `${ repoUrl }/-/raw/master/icon.png`;

	const item = {
		Author: manifest.Author,
		Name: manifest.Name,
		Punchline: '',
		Description: manifest.Description,
		InternalName: manifest.Name,
		AssemblyVersion: manifest.AssemblyVersion,
		RepoUrl: repoUrl,
		ApplicableVersion: manifest.ApplicableVersion,
		DalamudApiLevel: manifest.DalamudApiLevel,
		IconUrl: icon,
		ImageUrls: [
			icon
		],
		DownloadLinkInstall: link,
		DownloadLinkTesting: link,
		DownloadLinkUpdate: link,
		IsHide: false,
		IsTestingExclusive: false,
		DownloadCount: 0,
		LastUpdated: lastUpdated.toString()
	}
	out.push(item);

	const stringified = JSON.stringify(out, null, '\t');
	fs.writeFileSync('pluginmaster.json', stringified);
})()
.catch(console.log);
